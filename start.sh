#!/bin/bash

set -eu

echo "=> Patch config file"
CONFIG_FILE="/opt/ejabberd/conf/ejabberd.yml"

DOMAIN="${CLOUDRON_APP_DOMAIN}"

yq eval -i ".hosts[0]=\"${DOMAIN}\"" $CONFIG_FILE
# yq eval -i ".listen[3].tls=true" $CONFIG_FILE
yq eval -i ".listen[0].ip=\"0.0.0.0\"" $CONFIG_FILE
yq eval -i ".listen[1].ip=\"0.0.0.0\"" $CONFIG_FILE
yq eval -i ".listen[2].ip=\"0.0.0.0\"" $CONFIG_FILE
yq eval -i ".listen[3].ip=\"0.0.0.0\"" $CONFIG_FILE
yq eval -i ".listen[4].ip=\"0.0.0.0\"" $CONFIG_FILE
yq eval -i ".new_sql_schema=true" $CONFIG_FILE
yq eval -i ".acme.auto=false" $CONFIG_FILE
yq eval -i ".loglevel=\"debug\"" $CONFIG_FILE
# yq eval -i ".api_permissions.\"public commands\".who.access=\"all\"" $CONFIG_FILE
yq eval -i ".host_config.\"${DOMAIN}\".sql_type=\"pgsql\"" $CONFIG_FILE
yq eval -i ".host_config.\"${DOMAIN}\".sql_server=\"${CLOUDRON_POSTGRESQL_HOST}\"" $CONFIG_FILE
yq eval -i ".host_config.\"${DOMAIN}\".sql_database=\"${CLOUDRON_POSTGRESQL_DATABASE}\"" $CONFIG_FILE
yq eval -i ".host_config.\"${DOMAIN}\".sql_port=${CLOUDRON_POSTGRESQL_PORT}" $CONFIG_FILE
yq eval -i ".host_config.\"${DOMAIN}\".sql_username=\"${CLOUDRON_POSTGRESQL_USERNAME}\"" $CONFIG_FILE
yq eval -i ".host_config.\"${DOMAIN}\".sql_password=\"${CLOUDRON_POSTGRESQL_PASSWORD}\"" $CONFIG_FILE
yq eval -i ".host_config.\"${DOMAIN}\".auth_method[0]=\"sql\"" $CONFIG_FILE
yq eval -i "del(.ca_file)" $CONFIG_FILE
yq eval -i "del(.certfiles)" $CONFIG_FILE
yq eval -i ".certfiles[0]=\"/etc/certs/tls_cert.pem\"" $CONFIG_FILE
yq eval -i ".certfiles[1]=\"/etc/certs/tls_key.pem\"" $CONFIG_FILE

# for psql client
export PGPASSWORD="${CLOUDRON_POSTGRESQL_PASSWORD}"

echo "=> Import initial schema"
psql -f "/opt/ejabberd-${EJABBERD_VERSION}/lib/ejabberd-${EJABBERD_VERSION}/priv/sql/pg.new.sql" -h ${CLOUDRON_POSTGRESQL_HOST} -p ${CLOUDRON_POSTGRESQL_PORT} -U ${CLOUDRON_POSTGRESQL_USERNAME} -d ${CLOUDRON_POSTGRESQL_DATABASE}

echo "=> Start ejabberd"
/opt/ejabberd-${EJABBERD_VERSION}/bin/ejabberdctl foreground

echo "=> Ensure admin account"
# /opt/ejabberd-${EJABBERD_VERSION}/bin/ejabberdctl accounts register admin ${DOMAIN} changeme

echo "=> Tail logs"
# tail -F /opt/ejabberd/logs/*.log
