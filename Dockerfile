FROM cloudron/base:3.0.0@sha256:455c70428723e3a823198c57472785437eb6eab082e79b3ff04ea584faf46e92

RUN mkdir -p /app/pkg
WORKDIR /app/pkg

ENV EJABBERD_VERSION 21.07

RUN wget --quiet https://www.process-one.net/downloads/downloads-action.php?file=/${EJABBERD_VERSION}/ejabberd_${EJABBERD_VERSION}-0_amd64.deb -O ejabberd.deb && \
    dpkg -i ejabberd.deb && \
    rm ejabberd.deb

ADD start.sh /app/pkg/

CMD [ "/app/pkg/start.sh" ]
